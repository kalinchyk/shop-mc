# -*- coding: utf-8 -*-
import time
import mcrcon
import base64
import hashlib
# import mcstatus

from bottle import route, request, response, HTTPError


#
# Constants
#

STATUS_CREATED = 1
STATUS_UPDATED = 2
STATUS_DELETED = 3


@route('/whoami')
def _who_am_i():
    return {
        "object": "ADMIN" if _auth_get() else "CUSTOMER"
    }


#
# Session
#

def _auth_key():
    keys = u":".join((
        request.headers.get('User-Agent'),
        request.environ.get('REMOTE_ADDR'),
        app.config['minecraft.admin_username'],
        app.config['minecraft.admin_password']
    ))
    return base64.b64encode(hashlib.sha256(keys.encode('utf-8')).digest()).decode()


def _auth_get():
    return request.get_cookie(
        str(app.config['minecraft.cookie_name']),
        secret=app.config['minecraft.cookie_sign']
    ) == _auth_key()


def _auth_set():
    response.set_cookie(
        str(app.config['minecraft.cookie_name']),
        _auth_key(),
        secret=app.config['minecraft.cookie_sign'],
        path='/',
        expires=int(time.time()) + 100000,
        httponly=True
    )


def _auth_del():
    response.set_cookie(
        str(app.config['minecraft.cookie_name']),
        "",
        path='/',
        max_age=-1,
        httponly=True
    )


def _auth_chk():
    if not _auth_get():
        raise _auth_del() or HTTPError(401, "Access denied")


#
# Authorization
#

@route('/auth/ping', method='post')
def _auth_ping():
    _auth_chk()
    return {
        "message": "pong"
    }


@route('/auth/logout', method='post')
def _auth_logout():
    _auth_del()
    return {
        "message": "ok"
    }


@route('/auth/login', method='post')
def _auth_login():
    data = request.json
    if data['username'] == app.config['minecraft.admin_username'] \
            and data['password'] == app.config['minecraft.admin_password']:
        _auth_set()
        return {
            "message": "ok"
        }
    
    raise HTTPError(
        401,
        "Invalid username or password!"
    )


#
# Product history
#

@route('/private/product/<id>/history')
def _product_history_list(db, id):
    _auth_chk()
    history_list = []
    for item in db.execute('select id, product_id, amount, name, description, '
                           'color_id, detail, command, date, action_id '
                           'from product_history '
                           'where product_id = ? '
                           'order by id desc',
                           (id,)):
        history_list.append({
            "id": item[0], 
            "product_id": item[1],
            "amount": "%.2f" % item[2], 
            "name": item[3], 
            "description": item[4], 
            "color": _color_item(db, item[5]),
            "detail": item[6],
            "command": item[7],
            "date": time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(item[8])),
            "action": _action_item(db, item[9])
        })
    return {
        "object": "product_history", 
        "list": history_list
    }


def _product_history_item(db, id):
    for item in db.execute('select id, product_id, amount, name, description, '
                           'color_id, detail, command, date, action_id '
                           'from product_history '
                           'where id = ?',
                           (id,)):
        return {
            "id": item[0], 
            "product_id": item[1],
            "amount": "%.2f" % item[2], 
            "name": item[3], 
            "description": item[4], 
            "color": _color_item(db, item[5]),
            "detail": item[6],
            "command": item[7],
            "date": time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(item[8])),
            "action": _action_item(db, item[9])
        }
    return None


#
# Color
#

@route('/private/color/list', method='get')
def _color_list(db):
    _auth_chk()
    color_list = []
    for item in db.execute('select id, name, code '
                           'from color'):
        color_list.append({
            "id": item[0], 
            "name": item[1],
            "code": item[2]
        })
    return {
        "object": "color", 
        "list": color_list
    }


@route('/private/color/<id>', method='get')
def _color_item(db, id):
    _auth_chk()
    for item in db.execute('select id, name, code '
                           'from color '
                           'where id = ?',
                           (id,)):
        return {
            "id": item[0], 
            "name": item[1],
            "code": item[2]
        }
    raise HTTPError(
        400,
        "color with id '%s' not found" % id
    )


#
# Action
#

@route('/private/action/list', method='get')
def _action_list(db):
    _auth_chk()
    action_list = []
    for item in db.execute('select id, name, code '
                           'from action'):
        action_list.append({
            "id": item[0], 
            "name": item[1],
            "code": item[2]
        })
    return {
        "object": "action", 
        "list": action_list
    }


@route('/private/action/<id>', method='get')
def _action_item(db, id):
    _auth_chk()
    for item in db.execute('select id, name, code '
                           'from action '
                           'where id = ?',
                           (id,)):
        return {
            "id": item[0], 
            "name": item[1],
            "code": item[2]
        }
    raise HTTPError(
        400,
        "action with id '%s' not found" % id
    )


#
# Product
#

@route('/private/product/list', method='get')
def _product_list(db):
    _auth_chk()
    product_list = []
    for item in db.execute('select id, amount, name, description, color_id '
                           'from product'):
        product_list.append({
            "id": item[0], 
            "amount": "%.2f" % item[1], 
            "name": item[2], 
            "description": item[3], 
            "color": _color_item(db, item[4])
        })
    return {
        "object": "product", 
        "list": product_list
    }


@route('/private/product/<id>', method='get')
def _product_item(db, id):
    _auth_chk()
    for item in db.execute('select id, amount, name, description, color_id, detail, command '
                           'from product '
                           'where id = ?',
                           (id,)):
        return {
            "id": item[0], 
            "amount": "%.2f" % item[1], 
            "name": item[2], 
            "description": item[3], 
            "color": _color_item(db, item[4]),
            "detail": item[5],
            "command": item[6]
        }
    raise HTTPError(
        400,
        "product with id '%s' not found" % id
    )


def _product_check(data):
    result = {}
    fields = ["name", "color", "description", "detail", "amount", "command"]
    for item in fields:
        if item not in data:
            result[item] = "Параметр не передан на сервер"
            del(fields[item])
    for item in fields:
        if not data[item]:
            result[item] = "Обязательно для заполнения"
    
    if len(result) != 0: 
        return result

    return None if len(result) == 0 else result


@route('/private/product', method='post')
def _product_create(db):
    _auth_chk()
    data = request.json
    result = _product_check(data)
    if result is not None:
        raise HTTPError(400, {"fields": result})
    cur = db.cursor()
    cur.execute('insert into product '
                '(name, color_id, description, detail, amount, command) '
                'values (?, ?, ?, ?, ?, ?)',
                (data['name'], data['color']['id'], data['description'],
                 data['detail'], data['amount'], data['command']))
    product_id = cur.lastrowid
    cur.execute('insert into product_history '
                '(product_id, date, action_id, name, color_id, description, detail, amount, command) '
                'select id, ?, ?, name, color_id, description, detail, amount, command '
                'from product '
                'where id = ?',
                (time.time(), STATUS_CREATED, product_id))
    return {
        "message": "ok", 
        "rowid": product_id
    }


@route('/private/product/<product_id>', method='put')
def _product_update(db, product_id):
    _auth_chk()
    data = request.json
    result = _product_check(data)
    if result is not None:
        raise HTTPError(
            400,
            {
                "fields": result
            }
        )
    db.execute(
        'update product '
        'set name = ?, color_id = ?, description = ?, detail = ?, amount = ?, command = ? '
        'where id = ?',
        (
            data['name'],
            data['color']['id'],
            data['description'],
            data['detail'],
            data['amount'],
            data['command'],
            product_id
        )
    )
    db.execute(
        'insert into product_history '
        '(product_id, date, action_id, name, color_id, description, detail, amount, command) '
        'select id, ?, ?, name, color_id, description, detail, amount, command '
        'from product '
        'where id = ?',
        (
            time.time(),
            STATUS_UPDATED,
            product_id
        )
    )
    return {
        "message": "ok"
    }


@route('/private/product/<product_id>', method='delete')
def _product_delete(db, product_id):
    _auth_chk()
    db.execute(
        'insert into product_history '
        '(product_id, date, action_id, name, color_id, description, detail, amount, command) '
        'select id, ?, ?, name, color_id, description, detail, amount, command '
        'from product '
        'where id = ?',
        (
            time.time(),
            STATUS_DELETED,
            product_id
        )
    )
    db.execute(
        'delete from product '
        'where id = ?',
        (
            product_id,
        )
    )
    return {
        "message": "ok"
    }


#
# Donation
#

@route('/private/donate/list', method='get')
def _donate_list(db):
    _auth_chk()
    donate_list = []
    for item in db.execute('select id, user_login, status, product_history_id '
                           'from donate '
                           'order by id desc'):
        donate_list.append({
            "id": item[0], 
            "user_login": item[1], 
            "status": item[2], 
            "product_history": _product_history_item(db, item[3])
        })
    return {
        "object": "donate", 
        "list": donate_list
    }


@route('/private/donate/<id>', method='get')
def _donate_item(db, id):
    _auth_chk()
    for item in db.execute(
            'select id, user_login, status, product_history_id '
            'from donate '
            'where id = ?',
            (
                    id,
            )
    ):
        return {
            "id": item[0],
            "user_login": item[1],
            "status": item[2],
            "product_history": _product_history_item(db, item[3])
        }
    raise HTTPError(
        400,
        "donate with id '%s' not found" % id
    )


@route('/private/donate/<id>/<status>', method='patch')
def _donate_update(db, id, status):
    _auth_chk()
    db.execute(
        'update donate '
        'set status = ? '
        'where id = ?',
        (
            status,
            id
        )
    )
    db.execute(
        'insert into donate_history '
        '(donate_id, date, action_id, status) '
        'values (?, ?, ?, ?)',
        (
            id,
            time.time(),
            STATUS_UPDATED,
            status
        )
    )
    return {
        "message": "ok"
    }


#
# RCON
#

# @route('/private/server/status', method='get')
# def _server_status():
#     _auth_chk()
#     server = mcstatus.MinecraftServer.lookup(
#         "%s:%s" % (
#             app.config['minecraft.server_host'],
#             app.config['minecraft.server_query_port']
#         )
#     )
#     status = server.status()
#     return {
#         "status": status.raw
#     }


@route('/private/server/command', method='post')
def _server_rcon():
    _auth_chk()
    data = request.json
    server = mcrcon.MCRcon()
    server.connect(
        app.config['minecraft.server_host'],
        app.config['minecraft.server_rcon_port']
    )
    server.login(
        app.config['minecraft.server_rcon_password']
    )
    return server.command(
        data['command']
    )

