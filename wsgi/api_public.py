# -*- coding: utf-8 -*-
import time
import base64
import hashlib
# import mcstatus

from bottle import route, request, response, redirect, HTTPError


@route('/product/list', method='get')
def _product_list(db):
    product_list = []
    for item in db.execute(
            'select p.id, p.amount, p.name, p.description, c.code '
            'from product p, color c '
            'where p.color_id = c.id'
    ):
        product_list.append({
            "id": item[0], 
            "amount": "%.2f" % item[1], 
            "name": item[2], 
            "desc": item[3], 
            "color": item[4]
        })
    return {
        "object": "product", 
        "list": product_list
    }


@route('/product/<id>', method='get')
def _product_item(db, id):
    for item in db.execute(
            'select p.id, p.amount, p.name, p.description, c.code, p.detail '
            'from product p, color c '
            'where p.color_id = c.id '
            'and p.id = ?',
            (
                    id,
            )
    ):
        return {
            "id": item[0], 
            "amount": "%.2f" % item[1], 
            "name": item[2], 
            "desc": item[3], 
            "color": item[4],
            "description": item[5]
        }
    raise HTTPError(
        400,
        "product with id '%s' not found" % id
    )


# @route('/server/status', method='get')
# def _server_status():
#     server = mcstatus.MinecraftServer.lookup(
#         "%s:%s" % (
#             app.config['minecraft.server_host'],
#             app.config['minecraft.server_query_port']
#         )
#     )
#     status = server.status()
#     return {
#         'status': status.raw
#     }


@route('/donate', method='post')
def _donate_create(db):
    data = request.json
    product = _product_item(db, data['product_id'])
    session = data['session_id'] \
        if 'session_id' in data and data['session_id'] else \
        hashlib.sha256("%.25f" % time.time()).hexdigest()
    
    if app.config['minecraft.acquiring.type'] != "interkassa":
        raise HTTPError(400, "payments system not configured")

    cur = db.cursor()
    cur.execute(
        'insert into donate '
        '(user_login, status, product_history_id, ipaddr, useragent, session) '
        'values (?, \'new\', (select max(id) from product_history where product_id = ?), ?, ?, ?)',
        (
            data["user_login"],
            product['id'],
            request.environ.get('REMOTE_ADDR'),
            request.headers.get('User-Agent'),
            session
        )
    )
    
    donate_id = cur.lastrowid
    cur.execute(
        'insert into donate_history '
        '(donate_id, date, action_id, status, ipaddr) '
        'values (?, ?, ?, ?, ?)',
        (
            donate_id,
            time.time(),
            1,
            'new',
            request.environ.get('REMOTE_ADDR')
        )
    )

    order_id = "%s-%s" % (time.strftime("%y%j"), donate_id)
    cur.execute(
        'update donate '
        'set orderid = ? '
        'where id = ?',
        (
            order_id,
            donate_id
        )
    )

    fields = {
        "ik_co_id": app.config['minecraft.acquiring.id'],
        "ik_pm_no": order_id,
        "ik_am": product['amount'],
        "ik_desc": u"Пожертвование на развитие проекта",
        "ik_x_user": data["user_login"],
        "ik_x_product": data["product_id"],
        "ik_x_session": session,
        "ik_x_orderid": donate_id,
        "ik_cur": "UAH",
        "ik_suc_u": "%s/store/%s/status" % (app.config['minecraft.server_url'], order_id),
        "ik_suc_m": "GET",
        "ik_pnd_u": "%s/store/%s/status" % (app.config['minecraft.server_url'], order_id),
        "ik_pnd_m": "GET",
        "ik_fal_u": "%s/store/%s/status" % (app.config['minecraft.server_url'], order_id),
        "ik_fal_m": "GET",
        "ik_ia_u": "%s/api/process" % (app.config['minecraft.server_api']),
        "ik_ia_m": "POST"
    }

    keys = u':'.join(unicode(fields[k]) for k in sorted(fields.keys()))
    keys = u':'.join((keys, app.config['minecraft.acquiring.sign']))
    keys = base64.b64encode(hashlib.sha256(keys.encode('utf-8')).digest()).decode()
    fields['ik_sign'] = keys

    return {
        "object": app.config['minecraft.acquiring.type'], 
        "session_id": session,
        "form": {
            "method": app.config['minecraft.acquiring.form_method'], 
            "action": app.config['minecraft.acquiring.form_url'], 
            "list": fields
        }
    }


@route('/donate/<session>/list', method='get')
def _donate_item(db, session):
    donate_list = []
    for item in db.execute(
            'select d.id, d.user_login, d.status, ph.product_id, d.orderid '
            'from donate d, product_history ph '
            'where d.product_history_id = ph.id '
            'and d.session = ?',
            (
                    session,
            )
    ):
        donate_list.append({
            "id": item[0], 
            "user_login": item[1], 
            "status": item[2], 
            "product_id": item[3],
            "order_id": item[4]
        })
    return {
        "object": "donate", 
        "list": donate_list
    }


@route('/donate/<order_id>', method='get')
def _donate_item(db, order_id):
    for item in db.execute(
            'select d.id, d.user_login, d.status, ph.product_id, d.orderid '
            'from donate d, product_history ph '
            'where d.product_history_id = ph.id '
            'and d.orderid = ?',
            (
                    order_id,
            )
    ):
        return {
            "id": item[0], 
            "user_login": item[1], 
            "status": item[2], 
            "product_id": item[3],
            "order_id": item[4]
        }
    raise HTTPError(
        400,
        "donate with id '%s' not found" % id
    )
