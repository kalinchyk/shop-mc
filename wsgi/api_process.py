# -*- coding: utf-8 -*-
import time
import json
import mcrcon
import base64
import hashlib
# import mcstatus

from bottle import route, request, HTTPError

#
# Constants
#

STATUS_CREATED = 1
STATUS_UPDATED = 2
STATUS_DELETED = 3


@route('/process', method='post')
def _process(db):
    ipaddr = request.environ.get('REMOTE_ADDR').split(".")
    if int(ipaddr[0]) != 151 \
            or int(ipaddr[1]) != 80 \
            or int(ipaddr[2]) != 190 \
            or int(ipaddr[3]) not in range(97, 104 + 1):
        raise HTTPError(
            400,
            {
                "success": False,
                "message": "Ip адрес [%s] не входит в перечень разрешенных [151.80.190.97-104]" % ".".join(ipaddr)
            }
        )

    fields = {}
    sign = None
    prod = True
    for name in request.forms.dict.keys():
        val = request.forms.dict[name][0].decode('utf-8')
        if name == "ik_sign":
            sign = val
            continue
        if name == "ik_pw_via" \
                and val == "test_interkassa_test_xts":
            prod = False
        fields[name] = val

    keys = u':'.join(unicode(fields[k]) for k in sorted(fields.keys()))
    keys = u':'.join((keys, app.config['minecraft.acquiring.sign' + ('' if prod else '_test')]))
    keys = base64.b64encode(hashlib.sha256(keys.encode('utf-8')).digest()).decode()

    if sign != keys:
        raise HTTPError(
            400,
            {
                "success": False,
                "message": "Подпись платежа не прошла проверку"
            }
        )

    if app.config['minecraft.acquiring.id'] != fields["ik_co_id"]:
        raise HTTPError(
            400,
            {
                "success": False,
                "message": "Идентификатор кассы передан не корректно"
            }
        )

    command = None
    for data in db.execute(
            'select d.id, d.session, ph.command, d.user_login '
            'from donate d, product_history ph '
            'where d.product_history_id = ph.id '
            'and d.orderid = ?',
            (
                    fields["ik_pm_no"],
            )
    ):
        if fields['ik_x_orderid'] == str(data[0]) \
                and fields['ik_x_session'] == data[1]:
            command = data[2]
            command = command.replace('{user_login}', data[3])
            break
    else:
        raise HTTPError(
            400,
            {
                "success": False,
                "message": "Заявка с данным идентификатором не найдена"
            }
        )

    db.execute(
        'update donate '
        'set status = ? '
        'where orderid = ?',
        (
            fields["ik_inv_st"],
            fields["ik_pm_no"],
        )
    )

    db.execute(
        'insert into donate_history '
        '(date, action_id, donate_id, status, ipaddr, request) '
        'values (?, ?, ?, ?, ?, ?)',
        (
            time.time(),
            STATUS_UPDATED,
            fields['ik_x_orderid'],
            fields['ik_inv_st'],
            request.environ.get('REMOTE_ADDR'),
            json.dumps(
                fields
            )
        )
    )

    if fields['ik_inv_st'] == "success":
        _server_rcon(command)

    return {
        "success": True,
        "message": "ok"
    }


@route('/server/rcon/<command>', method='get')
def _server_rcon(command):
    server = mcrcon.MCRcon()
    server.connect(
        app.config['minecraft.server_host'],
        app.config['minecraft.server_rcon_port']
    )
    server.login(
        app.config['minecraft.server_rcon_password']
    )
    return server.command(
        command
    )

