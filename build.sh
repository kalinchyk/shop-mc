#!/usr/bin/env bash

rm ./minecraft/*
rm ./wsgi/*

find ../minecraft/dist/* -type f ! -name "*.gz" -exec cp -t ./minecraft "{}" \+
find ../api/src/* -type f ! -name "*.pyc" ! -name "*.db" ! -name "*.json" -exec cp -t ./wsgi "{}" \+
